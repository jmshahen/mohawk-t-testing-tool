#!/usr/bin/env python
#########################################
#		    ASASPTIME @2014				#
#	   Developed by Anh Truong			#
#		    tatruong@fbk.eu				#
#	    Fondazione Bruno Kessler		#
#########################################
import sys
import os
import shutil
import time

# The script takes three input arguments:
# 1. option
#if option = -rSA or -rNSA
# 2. File     the file should be started by "CONFIG maxnumofroles maxTimeslots\n GOAL role timeslot\n" and then can_assign or can_revoke...


option = sys.argv[1]
originfile = sys.argv[2]

def extractFile(oFile, tFile):   
    try:
        of = open(oFile,"r")
        tf = open(tFile,"w")
        theConfig = ""
        theGoal = ""    
        for line in of:
            if (line.find("CONFIG") > -1):
                lstCF = line.split(" ")   
        	theConfig = str.strip(lstCF[1]) + " " + str.strip(lstCF[2])
            elif (line.find("GOAL") > -1):
        	lstGoal = line.split(" ")   
        	theGoal = str.strip(lstGoal[1]) + " " + str.strip(lstGoal[2])         	
            else:
                tf.write(line)
                
        of.close()
        tf.close()
        return theConfig + " " + theGoal  
    except IOError:  
        return  "error" 

CFandGoal = extractFile(originfile, originfile + "_sys")
if (CFandGoal == "error"):
    print "No Input File Found!!!"
    sys.exit()

r2a = { 
1 : 'a', 2 : 'b', 3 : 'c', 4 : 'd' , 5 : 'e' , 6 : 'f', 7 : 'g', 
8 : 'h', 9 : 'i' , 10 : '@' , 11 : 'k' , 12 : 'l', 13 : 'm', 14 : 'n', 
15 : 'o', 16 : 'p', 17 : 'q', 18 : 'r', 19 : 's', 20 : 't', 21 : 'u', 
22 : 'v', 23 : 'w', 24 : '#', 25 : '$', 26 : 'z', 
27 : 'A', 28 : 'B', 29 : 'C', 30 : 'D' , 31 : 'E' , 32 : 'F', 33 : 'G', 
34 : 'H', 35 : 'I' , 36 : 'J' , 37 : 'K' , 38 : 'L', 39 : 'M', 40 : 'N', 
41 : 'O', 42 : 'P', 43 : 'Q', 44 : 'R', 45 : 'S', 46 : 'T', 47 : 'U', 
48 : 'V', 49 : 'W', 50 : 'X', 51 : 'Y', 52 : 'Z' 
}
os.system('echo "==========================================="' )
if (option == '-rSA') : 
    p = os.popen('./AGTSystem.py ' + originfile + "_sys" + ' ur ' + CFandGoal)
    for line in p.readlines():
	    
	if (line.find("UNSAFE") > -1):
	    os.system('echo "==========================================="' )
	    os.system('echo "System is UNSAFE!"' )
	    break
 	elif (line.find("SAFE") > -1):
            os.system('echo "==========================================="' )
	    os.system('echo "System is SAFE!"' )
	    break
	
elif (option == '-rNSA') :    
    p = os.popen('./AGTSystemNSA.py ' + originfile + "_sys" + ' ur ' + CFandGoal)
    for line in p.readlines():
	    
	if (line.find("UNSAFE") > -1):
	    os.system('echo "==========================================="' )
	    os.system('echo "System is UNSAFE!"' )
	    break
        elif (line.find("SAFE") > -1):
            os.system('echo "==========================================="' )
	    os.system('echo "System is SAFE!"' )
	    break


