package mohawk.testingtool.helper;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.concurrent.*;
import java.util.logging.Logger;

import mohawk.global.results.ExecutionResult;
import mohawk.global.results.TestingResult;
import mohawk.global.timing.TimingEvent;
import mohawk.testingtool.runner.RunTest;

public class ProcessHelper {
    public final static Logger logger = Logger.getLogger("mohawk");

    public Long TIMEOUT_SECONDS = (long) 60 * 5;// Default 5 minutes
    public TestingResult lastResult = null;
    public TimingEvent lastTiming = null;

    public TestingResult runProcess(RunTest runTest) {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        try {
            lastTiming = new TimingEvent();
            lastTiming.comment = "Process Helper";
            lastTiming.setStartTimeNow();

            // Setup Timeout Timer
            Future<ExecutionResult> future = executor.submit(runTest);

            logger.info("[RUNNING] Executing Process (" + runTest.getCommandLineString() + ")");
            ExecutionResult execResult = future.get(TIMEOUT_SECONDS, TimeUnit.SECONDS);
            // ExecutionResult execResult = executor.submit(runTest).get(TIMEOUT_SECONDS, TimeUnit.SECONDS);
            logger.info("[RUNNING] Finished Executing Process");

            lastTiming.setFinishTimeNow();
            lastResult = new TestingResult(execResult, lastTiming.duration(), runTest._specFile,
                    Integer.toString(runTest.execProcess.exitValue()), runTest.toString());
        } catch (TimeoutException e) {
            logger.warning("[TIMEOUT] Process has Timed Out after " + TIMEOUT_SECONDS + " seconds");

            lastTiming.setFinishTimeNow();
            lastTiming.failed();
            lastResult = new TestingResult(ExecutionResult.TIMEOUT, lastTiming.duration(), runTest._specFile, "timeout",
                    runTest.toString());
        } catch (OutOfMemoryError e) {
            logger.warning("[OUT OF MEMORY] Process has run out of memory");

            lastTiming.setFinishTimeNow();
            lastTiming.failed();
            lastResult = new TestingResult(ExecutionResult.OUT_OF_MEMORY, lastTiming.duration(), runTest._specFile,
                    "outofmemory", runTest.toString());
        } catch (InterruptedException e) {
            lastTiming.setFinishTimeNow();
            lastTiming.failed();

            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            logger.severe(errors.toString());
        } catch (ExecutionException e) {
            lastTiming.setFinishTimeNow();
            lastTiming.failed();

            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            logger.severe(errors.toString());
        }

        // Do not know if this is required
        executor.shutdown();

        return lastResult;
    }
}
