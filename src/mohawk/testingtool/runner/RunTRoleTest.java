package mohawk.testingtool.runner;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import mohawk.global.results.ExecutionResult;

public class RunTRoleTest extends RunTest {

    public RunTRoleTest(File specFile, String procPath) {
        super(specFile, procPath);
    }

    @Override
    public List<String> getCommandLineString() {
        return Arrays.asList(_procFile.getAbsolutePath(), _specFile.getAbsolutePath());
    }

    @Override
    public void setupCheckStr() {
        _checkReachable = ExecutionResult.GOAL_REACHABLE.getTRoleSearchString();
        _checkUnreachable = ExecutionResult.GOAL_UNREACHABLE.getTRoleSearchString();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Command Line: " + getCommandLineString());
        return sb.toString();
    }
}
