package mohawk.testingtool;

import java.util.ArrayList;
import java.util.Scanner;

import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.lang3.StringUtils;

public class TestingToolCUI {
    public static void main(String[] args) {
        try {
            TestingToolInstance inst = new TestingToolInstance();

            ArrayList<String> argv = new ArrayList<String>();
            String cmd = "";
            Scanner user_input = new Scanner(System.in);

            Options options = new Options();
            inst.setupOptions(options);
            HelpFormatter f = new HelpFormatter();
            f.printHelp(120, "mohawk", StringUtils.repeat("-", 80), options, StringUtils.repeat("-", 80), true);

            printCommonCommands();

            System.out.print("Enter Commandline Argument ('!exit' to run): ");
            while (true) {
                cmd = user_input.next();

                if (cmd.equals("!exit")) {
                    break;
                }
                argv.add(cmd);
            }
            user_input.close();

            inst.run(argv.toArray(new String[1]));
        } catch (Exception e) {
            e.printStackTrace();

            System.exit(-1);
        }
    }

    public static void printCommonCommands() {
        System.out.println("\n\n--- Common Commands ---");
        // {equallyRandom, 1/2Empty, 3/5Empty}
        System.out.println("-test_all !exit");

        System.out.println("");
        System.out.println("-test_trole -input data/TestsuiteB !exit");
        System.out.println("-test_trule -input data/TestsuiteB !exit");
        System.out.println("-test_mohawk -input data/TestsuiteB !exit");
        System.out.println("-test_asaptime_sa -input data/TestsuiteB !exit");
        System.out.println("-test_asaptime_nsa -input data/TestsuiteB !exit");

        System.out.println("");
        System.out.println("-test_trole -input data/positive !exit");
        System.out.println("-test_trule -input data/positive !exit");
        System.out.println("-test_mohawk -input data/positive !exit");
        System.out.println("-test_asaptime_sa -input data/positive !exit");
        System.out.println("-test_asaptime_nsa -input data/positive !exit");
        /*
        System.out.println("");
        System.out.println("-test_trole -input data/mixednocr !exit");
        System.out.println("-test_trule -input data/mixednocr !exit");
        System.out.println("-test_mohawk -input data/mixednocr !exit");
        System.out.println("-test_asaptime_sa -input data/mixednocr !exit");
        System.out.println("-test_asaptime_nsa -input data/mixednocr !exit");
        /*/
        System.out.println("");
        System.out.println("-test_trole -input data/mixed !exit");
        System.out.println("-test_trule -input data/mixed !exit");
        System.out.println("-test_mohawk -input data/mixed !exit");
        System.out.println("-test_asaptime_sa -input data/mixed !exit");
        System.out.println("-test_asaptime_nsa -input data/mixed !exit");
        /*
        System.out.println("");
        System.out.println("-test_mohawk -input data/TestsuiteC/AGTHos !exit");
        System.out.println("-test_asaptime_nsa -input data/TestsuiteC/AGTHos !exit");
        System.out.println("-test_asaptime_nsa -input data/TestsuiteCOrig/AGTHos !exit");
        System.out.println("-test_mohawk -input data/TestsuiteC/AGTUniv !exit");
        System.out.println("-test_asaptime_nsa -input data/TestsuiteC/AGTUniv !exit");
        System.out.println("-test_asaptime_nsa -input data/TestsuiteCOrig/AGTUniv !exit");
        */

        System.out.println("");
        System.out.println("-test_mohawk -input /media/jmshahen/Backup/Mahesh/easy !exit");
        System.out.println("-test_asaptime_nsa -input /media/jmshahen/Backup/Mahesh/easy !exit");
        System.out.println("-test_asaptime_sa -input /media/jmshahen/Backup/Mahesh/easy !exit");
    }
}
